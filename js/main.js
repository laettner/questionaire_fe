$(function() {

	function startApp() {
		window.app = window.app || {};	
		app.models = {};
		app.views = {};
		app.collections = {};
		app.routes = {
            "question" : "http://localhost:3000/question/",
        };
		
		app.views.mainView = new MainView();

        app.views.mainView.fetchQuestion();
    }
	
	var MainView = Backbone.View.extend({
		el : $('#qForm'),
		template : _.template( $('#questionTemplate').html() ),
		
		initialize : function() {
			this.curQuest = 1;},
		
		events : {

		},
		
		fetchQuestion : function(e) {
			var val = this.curQuest;
			var thisView = this;
			$.getJSON( app.routes["question"] + val, function(data) {
				this.el.html(this.template(data));
				this.rander();
			} ).error(function() {
				console.log('error occured during ajax call');
			}).complete(function() {

			});
		},

		nextQuestion : function(e) {
			this.curQuest += 1;
			this.fetchQuestion();
		},

        prevQuestion : function(e) {
            this.curQuest += 1;
            this.fetchQuestion();
        }
	});
	
	startApp();

});

